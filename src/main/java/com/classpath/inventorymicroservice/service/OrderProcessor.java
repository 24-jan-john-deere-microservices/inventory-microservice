package com.classpath.inventorymicroservice.service;

import com.classpath.inventorymicroservice.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;
import org.springframework.cloud.stream.messaging.Sink;


@Service
@Slf4j
public class OrderProcessor {

    @StreamListener(Sink.INPUT)
    public void processOrder(String order){
        log.info(" Processing the order :: {}", order);
    }
}